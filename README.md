# content-app-playwright

![Playwright Typescript et cucumber](./img/playwrightTypescriptCucumber.jpeg)

![playwright 1.32.0](https://img.shields.io/badge/palywright-1.32.0-brightgreen)
![nodeJS v16.10.0](https://img.shields.io/badge/nodeJS-v16.10.0-brightgreen)
![yarn 1.22.17](https://img.shields.io/badge/yarn-1.22.17-brightgreen)
![typescript 5.0.4](https://img.shields.io/badge/typescript-5.0.4-brightgreen)
![cucumber 9+](https://img.shields.io/badge/cucumber-9+-brightgreen)

## Lancer les tests

```
si besoin
$ yarn install

$ yarn run all
ou 
$ yarn run test

```

Une grande partie est inspiré de la source suivante:
https://conserto.pro/blog/playwright-loutil-qui-va-revolutionner-les-tests-end-to-end/ par Jean Franbcois Greffier.

## Pourquoi Playwright

Playwright est un projet open-source maintenu et sponsorisé par Microsoft. Il offre une API unifiée qui permet de 
piloter les principaux navigateurs web : Chromium, Webkit et Firefox. 
Le projet a commencé en 2020, mais se base sur plusieurs principes de Puppeteer, le célèbre outil permettant 
d’automatiser Chrome headless (sans interface graphique). 
En effet, une partie de l’équipe a quitté Google pour créer une solution plus ambitieuse, supportant les familles 
de navigateur les plus populaires. 
Playwright permet non seulement d’automatiser des navigateurs, mais aussi facilite les tests end-to-end 
fiables pour le web moderne.

https://medium.com/@jfgreffier/playwright-vs-the-world-c783e9bf4fc4

Parmi les points forts de Playwright :
* Rapide
* Robuste
* Facile

### Rapide

Playwright est sensiblement plus rapide que les autres solutions de test end-to-end concurrentes. Comparaison avec 
Cypress éprouvée.

### Robuste

Il est multi domaine(cypress l'ai devenu de les dernières versions de 2023) multi onglet. Il permet de naviguer sur
Salesforce ce qui n'avait pas été possible avec Cypress.

Les tests sur navigateurs sont souvent fragiles, typiquement le ça “marche sur ma machine” mais ça échoue
dans l’intégration continue.

Ce phénomène appelé flakiness est bien connu des personnes faisant du test end-to-end. 
Dépendant des conditions réseaux, de la machine cible, il est parfois tentant d’attendre (mais il ne faut pas...) 
une seconde ou deux “au cas où”. On peut aussi attendre qu’un élément soit présent.
Heureusement, Playwright s’occupe de tout ça automatiquement avec l’auto-wait. 
Avant de procéder à une action, le framework de test va s’assurer que les critères sont satisfaits. 
Par exemple, avant de cliquer sur un élément, il doit être : 

présent, visible, stable, ayant un listener pour le click, activé.

![Playwright robuste](./img/playwrightRobuste.png)

Quelques règles de l’auto-wait

### Facile
Les tests sont également faciles à débugger. 
Il est possible de dérouler les tests en pas à pas sur phpstorm (et Cucumber).
Mieux : le trace viewer de Playwright permet d’avoir des traces précises avec historique des commandes Playwright,
console du navigateur, traces réseau et snapshot du DOM (et non pas un screenshot.

Le trace viewer est vraiment une killer-app : à ma connaissance, aucun outil n’offre quelque chose d’aussi complet permettant d’analyser et de diagnostiquer après coup.

## Surcouche Cucumber pourquoi

Cucumber est adopté dans le groupe.

Après quelques essais, lors d'un POC, le choix a été fait de partir sur le template de Tally Barak

https://github.com/Tallyb/cucumber-playwright

lui même basé sur le repo https://github.com/hdorgeval/cucumber7-ts-starter/ de Henri d'Orgeval

![Playwright Cucumber](./img/cucumberSaisieNomDevis.png)

## Pourquoi Typescript + eslint

A completer eslint

ESLint est un linter JavaScript qui vous permet d'appliquer un ensemble de normes de style, de formatage 
et de codage pour votre base de code. 
Il examine votre code et vous indique quand vous ne suivez pas la norme que vous avez mise en place.